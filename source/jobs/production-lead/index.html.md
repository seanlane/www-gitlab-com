---
layout: job_page
title: "Production Lead"
---

The Production Lead reports to the Infrastructure Director. Production engineers
report to the Production Lead.

## Responsibilities

* Interviews applicants for production and developer positions
* Does 1:1's with all reports every 2-5 weeks (depending on the experience of the report)
* Is available for 1:1's on demand of the report
* Maintains an on-call schedule for 24x7 uptime for GitLab.com and GitHost.io
* Ensures high uptime and performance of GitLab.com and GitHost.io
* Prioritizes performance and scalability features with development team
* Ensures infrastructure issues are well-documented
* Provides necessary infrastructure (e.g. servers, databases, etc.) for company
* Coordinates with the team and product managers about plan for the work in the upcoming week
* Delivers input on promotions, function changes, demotions and firings in consultation with the CEO, CTO, and VP of Engineering
